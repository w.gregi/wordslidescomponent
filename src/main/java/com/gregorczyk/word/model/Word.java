package com.gregorczyk.word.model;

import java.util.StringJoiner;
import java.util.UUID;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Word {

    private final String value;
    private final UUID uuid;

    public Word(String value) {
        this.value = value;
        uuid = UUID.randomUUID();
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Word.class.getSimpleName() + "[", "]")
            .add("value='" + value + "'")
            .toString();
    }
}
