package com.gregorczyk.word.model;

import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SlideInteger {

    private Slide slide;
    private Integer integer;

    public Optional<Integer> getInteger() {
        return Optional.ofNullable(integer);
    }

    public String getSlide() {
        return slide.getSlideString();
    }

    public List<Word> getWords() {
        return slide.getWords();
    }

}
