package com.gregorczyk.word.model;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class Slide {

    private final List<Word> words;

    public String getSlideString() {
        return words.stream().map(Word::getValue).collect(Collectors.joining(" "));
    }
}
