package com.gregorczyk.word.imp;

import com.gregorczyk.word.api.StoreRepositoryInterface;
import com.gregorczyk.word.api.StoreServiceInterface;
import com.gregorczyk.word.model.Slide;
import com.gregorczyk.word.model.SlideInteger;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@Profile("prod")
public class StoreService implements StoreServiceInterface {

    private final StoreRepositoryInterface storeRepositoryInterface;
    private final Random random = new Random();

    public StoreService(StoreRepositoryInterface storeRepositoryInterface) {
        this.storeRepositoryInterface = storeRepositoryInterface;
    }

    @Override
    public SlideInteger findIntegerForSlide(Slide slide) {
        Integer randomInteger;

        if (storeRepositoryInterface.isExist(slide.getSlideString())) {
            loadSimulation();
            randomInteger = random.nextInt(10) + 1;
        } else {
            randomInteger = null;
        }
        return new SlideInteger(slide, randomInteger);
    }

    @Override
    public void loadSimulation() {
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            log.warn("Occurred problem with sleep thread method", e);
        }
    }
}
