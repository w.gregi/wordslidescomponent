package com.gregorczyk.word.imp;

import com.gregorczyk.word.api.SlidesFinderInterface;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class SlidesFinderController {

    private static final String SLIDE_OUT_PUT_FORMAT = "\"%s\" : <%d>";

    private final SlidesFinderInterface slidesFinder;

    public SlidesFinderController(SlidesFinderInterface slidesFinder) {
        this.slidesFinder = slidesFinder;
    }

    @GetMapping(value = "/find/{sentence}", produces = "text/plain")
    public String findSlides(@PathVariable String sentence) {
        Instant start = Instant.now();
        Map<String, Integer> stringIntegerMap = slidesFinder.findSlidesAndCalculateValues(sentence);
        log.info("The search slides for \"{}\" took {} millis", sentence, Duration.between(start, Instant.now()));

        return stringIntegerMap.entrySet().stream().map(slide -> String.format(SLIDE_OUT_PUT_FORMAT, slide.getKey(), slide.getValue()))
            .collect(Collectors.joining(", ", "[", "]"));
    }
}
