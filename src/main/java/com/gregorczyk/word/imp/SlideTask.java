package com.gregorczyk.word.imp;

import com.gregorczyk.word.api.StoreServiceInterface;
import com.gregorczyk.word.model.Slide;
import com.gregorczyk.word.model.SlideInteger;
import java.util.concurrent.Callable;
import lombok.AllArgsConstructor;

@AllArgsConstructor
class SlideTask implements Callable<SlideInteger> {

    private final Slide slide;
    private final StoreServiceInterface storeServiceInterface;

    @Override
    public SlideInteger call() {
        return storeServiceInterface.findIntegerForSlide(slide);
    }
}