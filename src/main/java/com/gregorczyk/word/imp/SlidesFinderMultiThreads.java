package com.gregorczyk.word.imp;

import static java.util.Collections.disjoint;

import com.gregorczyk.word.api.SlidesFinderInterface;
import com.gregorczyk.word.api.StoreServiceInterface;
import com.gregorczyk.word.model.Slide;
import com.gregorczyk.word.model.SlideInteger;
import com.gregorczyk.word.model.Word;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Log4j2
@Profile("multiThreads")
@Component
public class SlidesFinderMultiThreads implements SlidesFinderInterface {

    @Value("${slidesfinder.executors.threads}")
    private int numberOfThreads;
    private static final String SPACE = " ";
    private final StoreServiceInterface storeService;
    private ExecutorService executorService;

    @PostConstruct
    private void init() {
        executorService = Executors.newFixedThreadPool(numberOfThreads);
    }

    @Autowired
    public SlidesFinderMultiThreads(StoreServiceInterface storeService) {
        this.storeService = storeService;
    }

    public Map<String, Integer> findSlidesAndCalculateValues(String sentence) {
        Map<String, Integer> slideAndIntegerResultMap = new LinkedHashMap<>();
        Set<Word> wordsIncludedInResult = new HashSet<>();

        List<SlideInteger> slideIntegersFoundInStore = findSlideIntegersInStore(sentence);
        for (SlideInteger slideInteger : slideIntegersFoundInStore) {

            boolean isNotContainedAnyWordInSlideAndIntegerResultMap = disjoint(getDistinctWords(wordsIncludedInResult), slideInteger.getWords());

            if (isNotContainedAnyWordInSlideAndIntegerResultMap) {
                slideAndIntegerResultMap.put(slideInteger.getSlide(), slideInteger.getInteger().get());
                wordsIncludedInResult.addAll(slideInteger.getWords());
            }
        }
        return slideAndIntegerResultMap;
    }

    private List<Callable<SlideInteger>> prepareCallableSlideIntegers(List<Word> collect) {
        List<Callable<SlideInteger>> callableList = new LinkedList<>();

        for (int numberOfWords = collect.size() - 1; numberOfWords >= 1; numberOfWords--) {
            for (int positionOfWordInArry = 0; positionOfWordInArry <= collect.size() - numberOfWords; positionOfWordInArry++) {

                List<Word> words = getSlide(collect, positionOfWordInArry, numberOfWords);
                Slide slide = new Slide(words);

                callableList.add(new SlideTask(slide, storeService));
            }
        }
        return callableList;
    }

    private List<SlideInteger> findSlideIntegersInStore(String sentence) {

        List<Word> words = Arrays.stream(sentence.split(SPACE))
            .map(Word::new)
            .collect(Collectors.toList());

        List<Callable<SlideInteger>> callableSlideIntegers = prepareCallableSlideIntegers(words);

        List<SlideInteger> slideIntegersFoundInStore;
        try {
            slideIntegersFoundInStore = executorService.invokeAll(callableSlideIntegers)
                .stream()
                .map(getSlide())
                .filter(slide -> slide.getInteger().isPresent())
                .collect(Collectors.toList());
        } catch (InterruptedException e) {
            throw new IllegalStateException("executorService throw InterruptedException: ", e);
        }
        return slideIntegersFoundInStore;
    }

    private Function<Future<SlideInteger>, SlideInteger> getSlide() {
        return slideIntegerFuture -> {
            try {
                return slideIntegerFuture.get();
            } catch (InterruptedException ignored) {
                throw new IllegalStateException("Future<SlideInteger> throw InterruptedException: ", ignored);
            } catch (ExecutionException e) {
                throw new IllegalStateException("Future<SlideInteger> throw ExecutionException: ", e);
            }
        };
    }

    private List<Word> getDistinctWords(Set<Word> wordsAccepted) {
        return wordsAccepted.stream()
            .distinct()
            .collect(Collectors.toList());
    }

    private List<Word> getSlide(List<Word> words, int startAt, int numberOfWords) {
        return IntStream.range(startAt, startAt + numberOfWords)
            .mapToObj(words::get)
            .collect(Collectors.toList());
    }
}


