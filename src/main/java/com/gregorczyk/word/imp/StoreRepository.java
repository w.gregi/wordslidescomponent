package com.gregorczyk.word.imp;

import com.gregorczyk.word.api.StoreRepositoryInterface;
import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class StoreRepository implements StoreRepositoryInterface {

    private final Random random = new Random();

    @Override
    public boolean isExist(String slide) {
        return random.nextBoolean();
    }
}
