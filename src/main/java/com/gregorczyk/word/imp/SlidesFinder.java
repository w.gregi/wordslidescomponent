package com.gregorczyk.word.imp;

import static java.util.Collections.disjoint;

import com.gregorczyk.word.api.SlidesFinderInterface;
import com.gregorczyk.word.api.StoreServiceInterface;
import com.gregorczyk.word.model.Slide;
import com.gregorczyk.word.model.SlideInteger;
import com.gregorczyk.word.model.Word;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("singleThread")
@Component
public class SlidesFinder implements SlidesFinderInterface {

    private static final String SPACE = " ";
    private final StoreServiceInterface storeServiceInterface;

    @Autowired
    public SlidesFinder(StoreServiceInterface storeServiceInterface) {
        this.storeServiceInterface = storeServiceInterface;
    }

    @Override
    public Map<String, Integer> findSlidesAndCalculateValues(String sentence) {
        Map<String, Integer> result = new LinkedHashMap<>();

        String[] splitWords = sentence.split(SPACE);
        List<Word> collect = Arrays.stream(splitWords).map(Word::new).collect(Collectors.toList());

        Set<Word> slidesAccepted = new HashSet<>();

        for (int numberOfWords = collect.size() - 1; numberOfWords >= 1; numberOfWords--) {
            for (int positionOfWordInArry = 0; positionOfWordInArry <= collect.size() - numberOfWords; positionOfWordInArry++) {//position

                List<Word> words = getSlide(collect, positionOfWordInArry, numberOfWords);
                Slide slide = new Slide(words);

                SlideInteger integerSlide = storeServiceInterface.findIntegerForSlide(slide);

                if (isNotExistAnyWordsFromSlideInSlidesAccepted(slidesAccepted, slide)) {
                    integerSlide.getInteger().ifPresent(intSlide -> {
                        result.put(slide.getWords().stream().map(Word::getValue).collect(Collectors.joining(SPACE)), intSlide);
                        slidesAccepted.addAll(slide.getWords());
                    });
                }
            }
        }
        return result;
    }

    private boolean isNotExistAnyWordsFromSlideInSlidesAccepted(Set<Word> slidesAccepted, Slide slide) {
        return disjoint(getDistinctWords(slidesAccepted), slide.getWords());
    }


    private List<Word> getDistinctWords(Set<Word> wordsAccepted) {
        return wordsAccepted.stream()
            .distinct()
            .collect(Collectors.toList());
    }

    private List<Word> getSlide(List<Word> words, int startAt, int numberOfWords) {
        return IntStream.range(startAt, startAt + numberOfWords)
            .mapToObj(words::get)
            .collect(Collectors.toList());
    }
}


