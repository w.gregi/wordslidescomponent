package com.gregorczyk.word;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Log4j2
@EnableSwagger2
@SpringBootApplication
public class WordApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(WordApplication.class, args);
        String[] activeProfiles = run.getEnvironment().getActiveProfiles();
        for (String activeProfile : activeProfiles) {
            log.info(String.format("The application has been started with %s profile", activeProfile));
        }
    }

}
