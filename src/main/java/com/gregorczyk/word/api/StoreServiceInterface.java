package com.gregorczyk.word.api;

import com.gregorczyk.word.model.Slide;
import com.gregorczyk.word.model.SlideInteger;

public interface StoreServiceInterface {

    SlideInteger findIntegerForSlide(Slide slide);

    void loadSimulation();
}
