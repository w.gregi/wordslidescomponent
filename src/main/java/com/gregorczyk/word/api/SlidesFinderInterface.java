package com.gregorczyk.word.api;

import java.util.Map;

public interface SlidesFinderInterface {

    Map<String, Integer> findSlidesAndCalculateValues(String sentence);
}
