package com.gregorczyk.word.api;

import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepositoryInterface {

    boolean isExist(String slide);
}
