package com.gregorczyk.word;

import static org.mockito.ArgumentMatchers.any;

import com.gregorczyk.word.api.SlidesFinderInterface;
import com.gregorczyk.word.api.StoreRepositoryInterface;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@Log4j2
@SpringBootTest
@ActiveProfiles({"singleThread", "test"})
class SlidesFinderTest {

    @MockBean
    private StoreRepositoryInterface storeRepositoryInterface;

    @Autowired
    private SlidesFinderInterface slidesFinder;

    private static Stream<Arguments> sentencesAndSlides() {
        return Stream.of(
            Arguments.of("Mary went Mary's gone", new String[]{"went Mary's", "Mary"}),
            Arguments.of("Mary's gone Mary's gone", new String[]{"Mary's gone"}),
            Arguments.of("Mary's gone Mary's gone Mary's gone Mary's gone", new String[]{"Mary's gone"}),
            Arguments.of("Mary Mary's gone Mary's gone", new String[]{"Mary's gone", "Mary"}),
            Arguments.of("Mary Mary's Mary Mary's Mary Mary's gone Mary's gone", new String[]{"Mary's gone", "Mary"})
        );
    }

    @BeforeEach
    void init() {

        Mockito.when(storeRepositoryInterface.isExist(any())).thenReturn(false);
        Mockito.when(storeRepositoryInterface.isExist("Mary's gone")).thenReturn(true);
        Mockito.when(storeRepositoryInterface.isExist("Mary gone")).thenReturn(true);
        Mockito.when(storeRepositoryInterface.isExist("went Mary's")).thenReturn(true);
        Mockito.when(storeRepositoryInterface.isExist("Mary")).thenReturn(true);
        Mockito.when(storeRepositoryInterface.isExist("went")).thenReturn(true);
    }

    @ParameterizedTest
    @MethodSource("sentencesAndSlides")
    void shouldReturnCorrectSlides(String sentence, String[] sideExpected) {
        Instant start = Instant.now();
        Map<String, Integer> stringIntegerMap = slidesFinder.findSlidesAndCalculateValues(sentence);
        Assert.assertArrayEquals(sideExpected, stringIntegerMap.keySet().toArray());
        log.info("The search for \"{}\" took {} millis", sentence, Duration.between(start, Instant.now()).toMillis());
    }
}